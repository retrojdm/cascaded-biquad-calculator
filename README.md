# Cascaded Biquad  Calculator

Vue.js web tool to calculate coefficients for multiple layers of a cascaded biquadratic filter, and plot them.

## Cascaded layers

![ Cascaded layers](./screenshots/cascaded-layers.png)

Allows you to see the result of cascading multiple biquad filter stages.

The output is the product of the magnitudes of all biquad layers.

## Biquad filter layers

Plot and calculate coefficients for the following biquadratic filter types:

* Low Pass Filter
* High Pass Filter
* Band Pass Filter
* Notch
* Peak
* Low shelf
* High shelf

## Noise reduction layer

![Noise reduction layer](./screenshots/noise-reduction.gif)

This layer lets you visualize the effect of noise reduction at different signal strengths.

## Background image layer

![Background image layer](./screenshots/image-layer.png)

Choose an image to use as the background of the plot.

This can be useful if you're trying match the curve on a graph.

Custom size and position can be any [CSS length unit](https://www.w3schools.com/cssref/css_units.asp). Eg: % or px.

## Responsive / mobile friendly design
![Responsive / mobile friendly design](./screenshots/responsive.png)

## Open source

This project is open source and uses the GNU GPLv3 license.

GNU GPLv3 lets you do almost anything you want with this project, except distributing closed source versions.

See [COPYING.txt](./COPYING.txt) for the full license.

## Scripts

*   `npm install`: Project setup
*   `npm run serve`: Compiles and hot-reloads for development
*   `npm run dev`: Compiles dev version (no minification etc)
*   `npm run build`: Compiles and minifies for production
*   `npm run test`: Run your tests
*   `npm run lint`: Lints and fixes files
