process.env.VUE_APP_VERSION = require('./package.json').version

module.exports = {
  publicPath: './',
  css: {
    extract: true
  },
  devServer: {
    port: 8080,
    historyApiFallback: true
  },
  // Required for Toast UI charts.
  // Otherwise we get the following error:
  //
  //   [Vue warn]: You are using the runtime-only build of Vue where the template compiler is not available.
  //   Either pre-compile the templates into render functions, or use the compiler-included build.
  runtimeCompiler: true
}
