/**
 * Represents the coefficients to use in a biquad filter.
 * @class
 */
export default class Coefficients {
  /**
   * Initializes a new instance of the `Coefficients` class.
   * @constructor
   * @param {number} b0
   * @param {number} b1
   * @param {number} b2
   * @param {number} a0
   * @param {number} a1
   * @param {number} a2
   */
  constructor(b0, b1, b2, a0, a1, a2) {
    this.b0 = b0
    this.b1 = b1
    this.b2 = b2
    this.a0 = a0
    this.a1 = a1
    this.a2 = a2
  }

  /**
   * Copies the instance by value (instead of just by reference).
   * We need to clone complex objects when emitting updated props, to ensure Vue's reactivity.
   * @function
   * @returns {Coefficients} A cloned copy of the instance.
   */
  clone() {
    return new Coefficients(this.b0, this.b1, this.b2, this.a0, this.a1, this.a2)
  }

  /**
   * Mutates the values of this instance to normalize (resulting in a0 = 1).
   * @function
   */
  normalize() {
    this.b0 /= this.a0
    this.b1 /= this.a0
    this.b2 /= this.a0
    this.a1 /= this.a0
    this.a2 /= this.a0
    this.a0 = 1
  }

  /**
   * Generates a string representation of the coefficients, with one value per line.
   * Omits `a0` if the coefficients have been normalized.
   * @function
   * @returns {string}
   */
  toString() {
    let coeffs = { b0: this.b0, b1: this.b1, b2: this.b2, a0: this.a0, a1: this.a1, a2: this.a2 }

    if (this.a0 === 1) {
      delete coeffs.a0
    }

    return Object.entries(coeffs)
      .map((x) => `${x[0]} = ${x[1]}`)
      .join('\n')
  }
}
