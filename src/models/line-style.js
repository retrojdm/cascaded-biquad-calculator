/**
 * Represents a line stroke width, style, and color.
 * @class
 */
export default class LineStyle {
  /**
   * Initializes a new instance of the `LineStyle` class.
   * @constructor
   */
  constructor() {
    /** @property {number} width Line stroke width in pixels. */
    this.width = 2

    /** @property {number} width Line stroke style. [solid|dotted|dashed]. */
    this.style = 'solid'

    /** @property {number} width Line stroke color. */
    this.color = '#3366cc'
  }

  /**
   * ES6 classes don't support constructor overloading, so we use the builder pattern.
   * @function
   * @param {number} width Line stroke width in pixels.
   * @param {number} width Line stroke style. [solid|dotted|dashed].
   * @param {number} width Line stroke color.
   * @returns {LineStyle} The same instance with it's properties set.
   */
  withProperties(width, style, color) {
    this.width = width
    this.style = style
    this.color = color
    return this
  }

  /**
   * Copies the instance by value (instead of just by reference).
   * We need to clone complex objects when emitting updated props, to ensure Vue's reactivity.
   * @function
   * @returns {LineStyle} A cloned copy of the instance.
   */
  clone() {
    return new LineStyle().withProperties(this.width, this.style, this.color)
  }

  /**
   * For SVG lines.
   * See the CSS `stroke-dasharray` attribute.
   * @function
   * @returns {string}
   */
  strokeDashArray() {
    switch (this.style) {
      case 'solid':
        return ''

      case 'dotted':
        return `${this.width},${this.width}`

      case 'dashed':
        return `${this.width * 3},${this.width * 3}`
    }
  }
}
