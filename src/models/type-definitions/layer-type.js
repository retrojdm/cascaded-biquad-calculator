/**
 * Represents a single definition for a layer type.
 * @class
 */
export default class LayerType {
  /**
   * Initializes a new instance of the `LayerType` class.
   * @constructor
   * @param {string} code The unique code to reference this type with.
   * @param {string} displayName The name to display in drop-down lists etc.
   * @param {string} icon The name of a Bootstrap icon. Eg: 'bi-soundwave'
   * @param {boolean} allowMultiple Allow multiple layers of this type to be added to the list.
   * @param {boolean} addToEnd Add to the start or end of the list.
   */
  constructor(code, displayName, icon, allowMultiple, addToEnd) {
    /** @property {string} code The unique code to reference this type with. */
    this.code = code

    /** @property {string} displayName The name to display in drop-down lists etc. */
    this.displayName = displayName

    /** @property {string} icon The name of a Bootstrap icon. Eg: 'bi-soundwave' */
    this.icon = icon

    /** @property {boolean} allowMultiple Allow multiple layers of this type to be added to the list. */
    this.allowMultiple = allowMultiple

    /** @property {boolean} addToEnd Add to the start or end of the list. */
    this.addToEnd = addToEnd
  }

  /**
   * Gets a list of supported layer types.
   * @function
   * @returns {LayerType[]} An array of type definitions.
   */
  static getList() {
    return [
      new LayerType('biquad', 'Biquad', 'bi-soundwave', true, false),
      new LayerType('image', 'Background Image', 'bi-image', false, true),
      new LayerType('nr', 'Noise reduction', 'bi-boombox-fill', true, false),
      new LayerType('output', 'Cascaded Output', 'bi-diagram-3-fill', false, false)
    ]
  }
}
