/**
 * Represents a single definition for a biquad filter type.
 * @class
 */
export default class BiquadType {
  /**
   * Initializes a new instance of the `BiquadFormData` class.
   * @constructor
   * @param {string} code The unique code to reference this type with.
   * @param {string} displayName The name to display in drop-down lists etc.
   * @param {boolean} usesGain Enabled or disable the gain control on the form.
   */
  constructor(code, displayName, usesGain) {
    /** @property {string} code The unique code to reference this type with. */
    this.code = code

    /** @property {string} displayName The name to display in drop-down lists etc. */
    this.displayName = displayName

    /** @property {boolean} usesGain Enabled or disable the gain control on the form. */
    this.usesGain = usesGain
  }

  /**
   * Gets a list of supported biquad filter types.
   * @function
   * @returns {BiquadType[]} An array of type definitions.
   */
  static getList() {
    return [
      new BiquadType('LPF', 'Low Pass Filter', false),
      new BiquadType('HPF', 'High Pass Filter', false),
      new BiquadType('BPF', 'Band Pass Filter', false),
      new BiquadType('notch', 'Notch', false),
      new BiquadType('peakingEQ', 'Peak', true),
      new BiquadType('lowShelf', 'Low shelf', true),
      new BiquadType('highShelf', 'High shelf', true)
    ]
  }
}
