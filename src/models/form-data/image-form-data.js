/**
 * Represents the data in the `ImageForm` component.
 * @class
 */
export default class ImageFormData {
  /**
   * Initializes a new instance of the `ImageFormData` class.
   * @constructior
   */
  constructor() {
    /** @property {string} src The image source (either a URL or base64 encoded image data). */
    this.src = ''

    /** @property {string} size [auto|cover|contain|{width} {height}}]. See CSS background-size property. */
    this.size = 'contain'

    /** @property {string} position See CSS background-position property. */
    this.position = 'top left'
  }

  /**
   * ES6 classes don't support constructor overloading, so we use the builder pattern.
   * @function
   * @param {string} src The image source (either a URL or base64 encoded image data).
   * @param {string} size [auto|cover|contain|{width} {height}}]. See CSS background-size property.
   * @param {string} position See CSS background-position property.
   * @returns {ImageFormData} The same instance with it's properties set.
   */
  withProperties(src, size, position) {
    this.src = src
    this.size = size
    this.position = position
    return this
  }

  /**
   * Copies the instance by value (instead of just by reference).
   * We need to clone complex objects when emitting updated props, to ensure Vue's reactivity.
   * @function
   * @returns {ImageFormData} A cloned copy of the instance.
   */
  clone() {
    return new ImageFormData().withProperties(this.src, this.size, this.position)
  }
}
