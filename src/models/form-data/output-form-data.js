import LineStyle from '@/models/line-style.js'

/**
 * Represents the data in the `OutputForm` component.
 * @class
 */
export default class OutputFormData {
  /**
   * Initializes a new instance of the `OutputFormData` class.
   * @constructior
   */
  constructor() {
    this.lineStyle = new LineStyle().withProperties(4, 'solid', '#9933cc')
  }

  /**
   * ES6 classes don't support constructor overloading, so we use the builder pattern.
   * @param {LineStyle} lineStyle
   * @returns {OutputFormData} The same instance with it's properties set.
   */
  withLineStyle(lineStyle) {
    this.lineStyle = lineStyle.clone()
    return this
  }

  /**
   * Copies the instance by value (instead of just by reference).
   * We need to clone complex objects when emitting updated props, to ensure Vue's reactivity.
   * @function
   * @returns {OutputFormData} A cloned copy of the instance.
   */
  clone() {
    return new OutputFormData().withLineStyle(this.lineStyle.clone())
  }
}
