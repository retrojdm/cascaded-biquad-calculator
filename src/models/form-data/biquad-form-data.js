import DspHelper from '@/lib/dsp-helper.js'

import LineStyle from '@/models/line-style.js'

/**
 * @typedef {Object} Coefficients
 */

/**
 * Represents the data in the `BiquadForm` component.
 * @class
 */
export default class BiquadFormData {
  /**
   * Initializes a new instance of the `BiquadFormData` class.
   * @constructor
   */
  constructor() {
    /** @proprty {string} biquadTypeCode The code of a type of biquad filter. See the `BiquadType` class. */
    this.biquadTypeCode = 'LPF'

    /** @property {number} Fs Sample rate (Hz). */
    this.Fs = 44100

    /** @property {number} f0 Center frequency (Hz). */
    this.f0 = 1000

    /** @property {number} Q */
    this.Q = 0.7071

    /** @property {number} dBgain Gain (dB). */
    this.dBgain = 1

    /** @property {LineStyle} lineStyle Line width, style, and color. */
    this.lineStyle = new LineStyle()

    /** @property {Coefficients} coefficients Derrived values. */
    this.coefficients = null
  }

  /**
   * ES6 classes don't support constructor overloading, so we use the builder pattern.
   * @function
   * @param {string} biquadTypeCode The code of a type of biquad filter. See the `BiquadType` class.
   * @param {number} Fs Sample rate (Hz).
   * @param {number} f0 Center frequency (Hz).
   * @param {number} Q
   * @param {number} dBgain Gain (dB).
   * @param {LineStyle} lineStyle Line width, style, and color.
   * @returns {BiquadFormData} The same instance with it's properties set.
   */
  withProperties(biquadTypeCode, Fs, f0, Q, dBgain, lineStyle) {
    this.biquadTypeCode = biquadTypeCode
    this.Fs = Fs
    this.f0 = f0
    this.Q = Q
    this.dBgain = dBgain
    this.lineStyle = lineStyle.clone()

    this.caclulateCoefficients()

    return this
  }

  /**
   * Copies the instance by value (instead of just by reference).
   * We need to clone complex objects when emitting updated props, to ensure Vue's reactivity.
   * @function
   * @returns {BiquadFormData} A cloned copy of the instance.
   */
  clone() {
    let biquadFormData = new BiquadFormData().withProperties(
      this.biquadTypeCode,
      this.Fs,
      this.f0,
      this.Q,
      this.dBgain,
      this.lineStyle.clone()
    )

    return biquadFormData
  }

  /**
   * Calculates the coefficients for this biquad form's parameters.
   * @function
   * @returns {Coefficients} An object containing the coefficients.
   */
  caclulateCoefficients() {
    this.coefficients = DspHelper.calculateCoefficients(this.biquadTypeCode, this.Fs, this.f0, this.Q, this.dBgain)
  }
}
