import LineStyle from '@/models/line-style.js'

/**
 * Represents the data in the `NoiseReductionForm` component.
 * @class
 */
export default class NoiseReductionFormData {
  /**
   * Initializes a new instance of the `NoiseReductionFormData` class.
   * @constructior
   */
  constructor() {
    /** @property {number} signalStrength 0.0 - 1.0. The signal strength to interpolate the coeffieicents with. */
    this.signalStrength = 0.0

    /**
     * @property {number} minLevelDb
     * The first level should be used at this level, then progress through the levels up to a full signal at 0dB.
     */
    this.minLevelDb = -40

    /** @property {LineStyle} lineStyle Line width, style, and color. */
    this.lineStyle = new LineStyle()

    /**
     * @property {string} levelsJsonc
     * A JSON representation of a 3-dimensional array with the following dimensions:
     * - level: an index for each level profile (or "curve")
     * - stage: an index for each biquad filter to cascade
     * - coefficient: [b0, b1, b2, a1, a2]
     */
    this.levelsJsonc = `// # Dolby B cascased biquad filter coefficients
// Each decibel "level profile" consists of a high shelf and peak filter stage (which will be cascaded together).
// Each stage is an array of coefficients in the order [b0, b1, b2, a1, a2].

// Encoding
[
    // -40dB
    // weakest/quienest signal
    [
        // Peak (Fs: 7000, Q: 0.45, Gain: 0.5)
        [1.029051798507479, -0.46280255767546347, -0.009641573093132119, -0.46280255767546347, 0.01941022541434694],

        // High Shelf (Fs: 770, Q: 0.48, Gain: 10)
        [2.9520952734587085, -5.372708561644798, 2.4428126553844005, -1.6914099704573722, 0.7136093376556832]
    ],

    // -30dB
    [
        // Peak (Fs: 2700, Q: 0.5, Gain: 2.5)
        [1.0876263670188087, -1.3438990509698465, 0.38691194705332355, -1.3438990509698465, 0.4745383140721322],
        
        // High Shelf (Fs: 550, Q: 0.5, Gain: 6.5)
        [2.0486378234732054, -3.8136786099955464, 1.7748555129768877, -1.8018613974557536, 0.8116761239103001]
    ],

    // -20dB
    [
        // Peak (Fs: 800, Q: 0.3, Gain: 2.0)
        [1.0406388961293522, -1.6728003619971796, 0.6454568343147628, -1.6728003619971796, 0.686095730444115],
        
        // High Shelf (Fs: 650, Q: 0.7071, Gain: 2.7)
        [1.3493650958622385, -2.518677330846604, 1.1805955245275759, -1.8442412300294855, 0.8555245195726954]
    ],

    // -10dB
    [
        // Peak (Fs: 400, Q: 0.2, Gain: 0)
        [1, -1.7252327976697994, 0.7286438814988032, -1.7252327976697994, 0.7286438814988032],

        // High Shelf (Fs: 400, Q: 0.5, Gain: 1.4)
        [1.16914038448421, -2.2012543944261034, 1.0361289741774533, -1.8732725091298674, 0.8772874733654276]
    ],
    // 0dB
    // strongest/loudest signal
    [
        // Peak (Fs: 400, Q: 0.2, Gain: 0)
        [1, -1.7252327976697994, 0.7286438814988032, -1.7252327976697994, 0.7286438814988032],

        // High Shelf (Fs: 400, Q: 0.5, Gain: 0)
        [1, -1.8781250116349846, 0.8818383898322278, -1.8781250116349846, 0.8818383898322278]
    ]
]`
  }

  /**
   * ES6 classes don't support constructor overloading, so we use the builder pattern.
   * @function
   * @param {number} signalStrength 0.0 - 1.0. The signal strength to interpolate the coeffieicents with.
   * @param {number} minLevelDb
   * The first level should be used at this level, then progress through the levels up to a full signal at 0dB.
   * @param {LineStyle} lineStyle Line width, style, and color.
   * @param {string} levelsJsonc
   * A JSON representation of a 3-dimensional array with the following dimensions:
   * - level: an index for each level profile (or "curve"), with level 0 being for the lowest level.
   * - stage: an index for each biquad filter to cascade
   * - coefficient: [b0, b1, b2, a1, a2]
   * @returns {NoiseReductionFormData} The same instance with it's properties set.
   */
  withProperties(signalStrength, minLevelDb, lineStyle, levelsJsonc) {
    this.signalStrength = signalStrength
    this.minLevelDb = minLevelDb
    this.lineStyle = lineStyle
    this.levelsJsonc = levelsJsonc
    return this
  }

  /**
   * Copies the instance by value (instead of just by reference).
   * We need to clone complex objects when emitting updated props, to ensure Vue's reactivity.
   * @function
   * @returns {NoiseReductionFormData} A cloned copy of the instance.
   */
  clone() {
    return new NoiseReductionFormData().withProperties(
      this.signalStrength,
      this.minLevelDb,
      this.lineStyle,
      this.levelsJsonc
    )
  }
}
