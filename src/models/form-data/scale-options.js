/**
 * Represents the data in the `ScaleOptions` component.
 * @class
 */
export default class ScaleOptions {
  /**
   * Initializes a new instance of the `ScaleOptions` class.
   * @constructior
   */
  constructor() {
    /** @property {number} Fs The sampling frequency (AKA the sample rate or sample size). */
    this.Fs = 40000

    /**
     * @property {boolean} horizontalScaleLog
     * A value indicating whether the x-axis of the plot is a log (or linear) scale.
     */
    this.horizontalScaleLog = true

    /** @property {boolean} verticalScaleDb A value indicating whether values are in dB (or frequency magnitude). */
    this.verticalScaleDb = true

    /** @property {boolean} verticalScaleAuto A value indicating whether minY and maxY are auto-calculated. */
    this.verticalScaleAuto = true

    /** @property {number} minY The lower bounds of the Y-axis domain. */
    this.minY = -10

    /** @property {number} maxY The upper bounds of the Y-axis domain. */
    this.maxY = 10
  }

  /**
   * ES6 classes don't support constructor overloading, so we use the builder pattern.
   * @function
   * @param {number} Fs The sampling frequency (AKA the sample rate or sample size).
   * @param {boolean} horizontalScaleLog A value indicating whether the x-axis of the plot is a log (or linear) scale.
   * @param {boolean} verticalScaleDb A value indicating whether values are in dB (or frequency magnitude).
   * @param {boolean} verticalScaleAuto A value indicating whether minY and maxY are auto-calculated.
   * @param {number} minY The lower bounds of the Y-axis domain.
   * @param {number} maxY The upper bounds of the Y-axis domain.
   * @returns {ScaleOptions} The same instance with it's properties set.
   */
  withProperties(Fs, horizontalScaleLog, verticalScaleDb, verticalScaleAuto, minY, maxY) {
    this.Fs = Fs
    this.horizontalScaleLog = horizontalScaleLog
    this.verticalScaleDb = verticalScaleDb
    this.verticalScaleAuto = verticalScaleAuto
    this.minY = minY
    this.maxY = maxY
    return this
  }

  /**
   * Copies the instance by value (instead of just by reference).
   * We need to clone complex objects when emitting updated props, to ensure Vue's reactivity.
   * @function
   * @returns {ScaleOptions} A cloned copy of the instance.
   */
  clone() {
    return new ScaleOptions().withProperties(
      this.Fs,
      this.horizontalScaleLog,
      this.verticalScaleDb,
      this.verticalScaleAuto,
      this.minY,
      this.maxY
    )
  }
}
