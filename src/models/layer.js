import BiquadFormData from '@/models/form-data/biquad-form-data.js'
import ImageFormData from '@/models/form-data/image-form-data.js'
import NoiseReductionFormData from '@/models/form-data/noise-reduction-form-data.js'
import OutputFormData from '@/models/form-data/output-form-data.js'
import BiquadType from '@/models/type-definitions/biquad-type.js'
import LayerType from '@/models/type-definitions/layer-type.js'

/**
 * Represents a single layer in the UI, which the plot lines etc are derived from.
 * @class
 */
export default class Layer {
  /**
   * Initializes a new instance of the `Layer` class.
   * @constructor
   * @param {string} layerTypeCode Code of a supported type. Eg: 'biquad'. See the `LayerType` class.
   * @param {boolean} expanded Is the layer form expanded or collapsed?
   * @param {boolean} visibled Is the layer visible on the plot?
   */
  constructor(layerTypeCode, expanded, visibled) {
    /** @property {string} layerTypeCode Code of a supported type. Eg: 'biquad'. See the `LayerType` class. */
    this.layerTypeCode = layerTypeCode

    /** @property {boolean} expanded Is the layer form expanded or collapsed? */
    this.expanded = expanded

    /** @property {boolean} visibled Is the layer visible on the plot? */
    this.visible = visibled

    /** @property {object} formData The form data, which depends on the layer type. */
    this.formData = null

    /** @private @property {LayerType[]} _layerTypes Layer type definitions. */
    this._layerTypes = LayerType.getList()

    /** @private @property {BiquadType[]} _biquadTypes Biquad type definitions. */
    this._biquadTypes = BiquadType.getList()
  }

  /**
   * ES6 classes don't support constructor overloading, so we use the builder pattern.
   * @function
   * @param {object} formData The form data, which depends on the layer type.
   * @returns The same instance with it's properties set.
   */
  withFormData(formData) {
    this.formData = formData
    return this
  }

  /**
   * ES6 classes don't support constructor overloading, so we use the builder pattern.
   * @function
   * @returns The same instance with it's properties set to default values, depending on the layer type.
   */
  withDefaultFormData() {
    switch (this.layerTypeCode) {
      case 'biquad':
        this.formData = new BiquadFormData()
        break

      case 'image':
        this.formData = new ImageFormData()
        break

      case 'nr':
        this.formData = new NoiseReductionFormData()
        break

      case 'output':
        this.formData = new OutputFormData()
        break
    }

    return this
  }

  /**
   * Copies the instance by value (instead of just by reference).
   * We need to clone complex objects when emitting updated props, to ensure Vue's reactivity.
   * @function
   * @returns {Layer} A cloned copy of the instance.
   */
  clone() {
    return new Layer(this.layerTypeCode, this.expanded, this.visible).withFormData(this.formData.clone())
  }

  /**
   * Gets a title to use for the layer heading.
   * @function
   * @returns {string}
   */
  getTitle() {
    let title = this._layerTypes.find((x) => x.code === this.layerTypeCode).displayName
    if (this.layerTypeCode === 'biquad') {
      title += ': ' + this._biquadTypes.find((x) => x.code === this.formData.biquadTypeCode).displayName
    }
    return title
  }

  /**
   * Returns `true` if the layer can be plotted as a line.
   * @function
   * @returns {boolean}
   */
  isPlottable() {
    return ['biquad', 'nr', 'output'].includes(this.layerTypeCode)
  }
}
