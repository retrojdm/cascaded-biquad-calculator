import DspHelper from '@/lib/dsp-helper.js'
import NoiseReductionHelper from '@/lib/noise-reduction-helper.js'

/**
 * Plot data generator static helper class.
 * @class
 */
export default class PlotDataGenerator {
  /**
   * Generates an array of data for plotting on a graph.
   * @
   * @param {number} Fs Sample rate (Hz).
   * @param {Layer[]} layers An array of Layer objects.
   * @param {number} rangeLength The number of data points to generate.
   * @param {boolean} horizontalScaleLog A value indicating whether the x-axis of the plot is a log (or linear) scale.
   * @param {boolean} verticalScaleDb A value indicating whether values are in dB (or frequency magnitude).
   * @returns {object[]}
   * An array of objects with a frequency (x) and separate magnitude (y) for each plottable and visible layer.
   * @example
   * [
   *   {
   *     x: 955.3791204268958,
   *     y0: 1.181706800869413,
   *     y1: 1.1214774887691779,
   *     y2: 1.0537053241847385
   *   }
   * ]
   */
  static generateData(Fs, layers, rangeLength, horizontalScaleLog, verticalScaleDb) {
    const frequencies = horizontalScaleLog
      ? PlotDataGenerator._generateScaleLog(20, Fs / 2, rangeLength)
      : PlotDataGenerator._generateScaleLinear(20, Fs / 2, rangeLength)

    return frequencies.map((f) => PlotDataGenerator._generateElement(Fs, layers, verticalScaleDb, f))
  }

  /**
   * Finds the min and max Y values for all series in the data set.
   * @function
   * @param {object[]} data The dataset.
   * @param {number} lineCount The number of series.
   * @returns {object} An object with minY and maxY properties.
   */
  static findVerticalBounds(data, lineCount) {
    let minY = Infinity
    let maxY = -Infinity
    data.forEach((element) => {
      for (let i = 0; i < lineCount; i++) {
        let y = element[`y${i}`]
        minY = Math.min(y, minY)
        maxY = Math.max(y, maxY)
      }
    })

    // Round to the nearest 4 decimal places, so that rounding up and down doesn't jump around with precision errors.
    minY = +minY.toFixed(4)
    maxY = +maxY.toFixed(4)

    // Now round up/down to the nearest one decimal place.
    minY = Math.floor(minY * 10) / 10
    maxY = Math.ceil(maxY * 10) / 10

    // Finally, constrain to sane limits since some filters can get very close to zero magnitude, which can become a
    // very large negtive value in decibels.
    // Note: If +/-100 is too narrow a range, the user can always enter ridiculous values manually.
    minY = Math.max(-100, minY)
    maxY = Math.min(100, maxY)

    return {
      minY: minY,
      maxY: maxY
    }
  }

  /**
   * Generates an array of numbers on a log scale.
   * @private
   * @function
   * @param {number} frequencyMin The lowest frequency in the range.
   * @param {number} frequencyMax The highest frequency in the range.
   * @param {number} rangeLength The number of data points to generate.
   * @returns {number[]} An array of frequency values.
   */
  static _generateScaleLinear(frequencyMin, frequencyMax, rangeLength) {
    var result = []
    var step = (frequencyMax - frequencyMin) / (rangeLength - 1)
    for (var i = 0; i < rangeLength; i++) {
      result.push(frequencyMin + step * i)
    }
    return result
  }

  /**
   * Generates an array of numbers on a linear scale.
   * @private
   * @function
   * @param {number} frequencyMin The lowest frequency in the range.
   * @param {number} frequencyMax The highest frequency in the range.
   * @param {number} rangeLength The number of data points to generate.
   * @returns {number[]} An array of frequency values.
   */
  static _generateScaleLog(frequencyMin, frequencyMax, rangeLength) {
    var f = Math.pow(frequencyMax / frequencyMin, 1 / --rangeLength)
    var result = [frequencyMin]
    while (--rangeLength) {
      result.push(result[result.length - 1] * f)
    }
    result.push(frequencyMax)
    return result
  }

  /**
   * Generates a single element for the plot data.
   * @private
   * @function
   * @param {number} Fs Sample rate (Hz).
   * @param {Layer[]} layers An array of Layer objects.
   * @param {boolean} verticalScaleDb A value indicating whether values are in dB (or frequency magnitude).
   * @param {number} f the fequency to generate the element for.
   * @returns {object}
   * An object with a frequency (x) and separate magnitude (y) for each plottable and visible layer.
   * @example
   * {
   *   x: 955.3791204268958,
   *   y0: 1.181706800869413,
   *   y1: 1.1214774887691779,
   *   y2: 1.0537053241847385
   * }
   */
  static _generateElement(Fs, layers, verticalScaleDb, f) {
    let element = {
      x: f
    }

    let magnitude = null
    let cascaded = 1
    let keyIndex = 0
    let outputKeys = []

    // Calculate magnitude responses for individual biquad layers.
    for (let i = 0; i < layers.length; i++) {
      let layer = layers[i]
      if (!layer.isPlottable()) {
        continue
      }

      let key = `y${keyIndex}`
      let formData
      let c

      switch (layer.layerTypeCode) {
        case 'biquad':
          formData = layer.formData
          c = formData.coefficients
          magnitude = DspHelper.calculateMagnitudeResponse(c.b0, c.b1, c.b2, c.a1, c.a2, Fs, f)
          cascaded *= magnitude
          if (layer.visible) {
            element[key] = verticalScaleDb ? DspHelper.toDb(magnitude) : magnitude
            keyIndex++
          }
          break

        case 'nr':
          formData = layer.formData
          magnitude = NoiseReductionHelper.calculateMagnitudeResponse(
            formData.signalStrength,
            formData.levelsJsonc,
            Fs,
            f
          )
          cascaded *= magnitude
          if (layer.visible) {
            element[key] = verticalScaleDb ? DspHelper.toDb(magnitude) : magnitude
            keyIndex++
          }
          break

        case 'output':
          // This is a placeholder.
          // We can't just use the cascaded value yet, because we might not have encountered all biquad layers.
          if (layer.visible) {
            element[key] = null
            outputKeys.push(key)
            keyIndex++
          }
          break
      }
    }

    // Set the value of the output layers with the cascaded value.
    for (let key of outputKeys) {
      element[key] = verticalScaleDb ? DspHelper.toDb(cascaded) : cascaded
    }

    return element
  }
}
