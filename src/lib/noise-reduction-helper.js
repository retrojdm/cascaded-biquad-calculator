import DspHelper from '@/lib/dsp-helper.js'

/**
 * Noise reduction static helper class.
 * @class
 */
export default class NoiseReductionHelper {
  /**
   * @function
   * @param {number} signalStrength 0.0 - 1.0. The signal strength to interpolate the coeffieicents with.
   * @param {string} levelsJsonc
   * A JSON representation of a 3-dimensional array with the following dimensions:
   * - level: an index for each level profile (or "curve"), with level 0 being for the lowest level.
   * - stage: an index for each biquad filter to cascade
   * - coefficient: [b0, b1, b2, a1, a2]
   * @param {number} Fs Sample rate (Hz).
   * @param {number} f the fequency to calculate the magnitude for.
   * @returns {number}
   */
  static calculateMagnitudeResponse(signalStrength, levelsJsonc, Fs, f) {
    // Remove comments.
    const json = levelsJsonc.replace(/\\"|"(?:\\"|[^"])*"|(\/\/.*|\/\*[\s\S]*?\*\/)/g, (m, g) => (g ? '' : m))
    const levels = JSON.parse(json)
    const proportionalIndex = signalStrength * (levels.length - 1)
    const lowerIndex = Math.floor(proportionalIndex)
    const upperIndex = Math.ceil(proportionalIndex)
    const lowerLevel = levels[lowerIndex]
    const upperLevel = levels[upperIndex]
    const mix = NoiseReductionHelper._fractionalPart(proportionalIndex)

    // Assumed to be the same as upperLevel.length.
    const stages = lowerLevel.length

    let cascaded = 1
    for (let stage = 0; stage < stages; stage++) {
      let c = new Array(5).fill(0).map((x, i) => {
        let lowerCoefficient = lowerLevel[stage][i]
        let upperCoefficient = upperLevel[stage][i]

        // Interpolate.
        return lowerCoefficient + (upperCoefficient - lowerCoefficient) * mix
      })

      let magnitude = DspHelper.calculateMagnitudeResponse(c[0], c[1], c[2], c[3], c[4], Fs, f)
      cascaded *= magnitude
    }

    return cascaded
  }

  /**
   * Gets the fractional part of a number.
   * @private
   * @function
   * @param {number} value The number to get the fractional part of.
   * @returns {number} The fractional part of a number.
   */
  static _fractionalPart(value) {
    const n = Math.abs(value)
    return n - Math.floor(n)
  }
}
