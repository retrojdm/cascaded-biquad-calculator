/**
 * Array static helper class.
 * @class
 */
export default class ArrayHelper {
  /**
   * See https://www.w3docs.com/snippets/javascript/how-to-move-an-array-element-from-one-array-position-to-another.html
   * @function
   * @param {objext[]} arr The array to manipulate.
   * @param {number} oldIndex
   * @param {number} newIndex
   * @returns {objext[]}
   */
  static moveElement(arr, oldIndex, newIndex) {
    if (newIndex >= arr.length) {
      let i = newIndex - arr.length + 1
      while (i--) {
        arr.push(undefined)
      }
    }
    arr.splice(newIndex, 0, arr.splice(oldIndex, 1)[0])
    return arr
  }
}
