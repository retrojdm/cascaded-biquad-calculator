import Coefficients from '@/models/coefficients.js'

/**
 * Digital Signal Processing (DSP) static helper class.
 * @class
 */
export default class DspHelper {
  /**
   * Calculates the coefficients, normalized.
   *
   * Note: This code could be optimised, but for the sake of readability the equations are directly copied from the
   * Audio EQ cookbook. See https://www.w3.org/TR/audio-eq-cookbook/
   * @function
   * @param {string} type
   * The code of a supported type. Eg: 'LPF', 'HPF', 'peakingEQ', etc. See the `BiquadType` class.
   * @param {number} Fs
   * The sampling frequency (AKA the sample rate or sample size).
   * @param {number} f0
   * Center Frequency or Corner Frequency, or shelf midpoint frequency, depending on which filter type.
   * The "significant frequency". "wherever it's happenin', man."
   * @param {number} Q
   * The EE kind of definition, except for peakingEQ in which A⋅Q is the classic EE Q.
   * That adjustment in definition was made so that a boost of N dB followed by a cut of N dB for identical Q and
   * f0/Fs results in a precisely flat unity gain filter or "wire".
   * @param {number} dBgain
   * Used only for peaking and shelving filters.
   * @returns {Coefficients}
   */
  static calculateCoefficients(type, Fs, f0, Q, dBgain) {
    let coefficients

    switch (type) {
      case 'LPF':
        coefficients = DspHelper._lpf(Fs, f0, Q)
        break

      case 'HPF':
        coefficients = DspHelper._hpf(Fs, f0, Q)
        break

      case 'BPF':
        coefficients = DspHelper._bpf(Fs, f0, Q)
        break

      case 'notch':
        coefficients = DspHelper._notch(Fs, f0, Q)
        break

      case 'peakingEQ':
        coefficients = DspHelper._peakingEq(Fs, f0, Q, dBgain)
        break

      case 'lowShelf':
        coefficients = DspHelper._lowShelf(Fs, f0, Q, dBgain)
        break

      case 'highShelf':
        coefficients = DspHelper._highShelf(Fs, f0, Q, dBgain)
        break
    }

    coefficients.normalize()

    return coefficients
  }

  /**
   * Get the magnitude response for a single frequency.
   * It's assumed that the coefficients have been normalized.
   * @function
   * @param {number} b0 b0 coefficient (pole)
   * @param {number} b1 b0 coefficient (pole)
   * @param {number} b2 b0 coefficient (pole)
   * @param {number} a1 a1 coefficient (zero).
   * @param {number} a2 a2 coefficient (zero).
   * @param {number} Fs The sampling frequency (AKA the sample rate or sample size).
   * @param {number} f The frequency to calculate the magnitude for.
   * @returns {number} The magnitude response (decimal value, not dB).
   */
  static calculateMagnitudeResponse(b0, b1, b2, a1, a2, Fs, f) {
    const w = 2 * Math.PI * (f / Fs)

    // See equation (18) from http://rs-met.com/documents/dsp/BasicDigitalFilters.pdf
    const cosW = Math.cos(w)
    const cos2W = Math.cos(2 * w)
    const numerator = b0 * b0 + b1 * b1 + b2 * b2 + 2 * (b0 * b1 + b1 * b2) * cosW + 2 * b0 * b2 * cos2W
    const denominator = 1 + a1 * a1 + a2 * a2 + 2 * (a1 + a1 * a2) * cosW + 2 * a2 * cos2W

    // There's a rounding error that causes a negative value here, resulting in NaN when trying to find the square root
    // of a negative number.
    const magnitudeSquared = numerator / denominator
    if (magnitudeSquared <= 0) {
      // We can't use zero, because that will convert to -Infinity dB.
      return Number.MIN_VALUE
    }

    return Math.sqrt(magnitudeSquared)
  }

  /**
   * Convert frequency response magnitude to decibels.
   * @function
   * @param {number} magnitude Frequency response.
   * @returns {number} Signal strength in Db.
   */
  static toDb(magnitude) {
    return 20 * Math.log10(magnitude)
  }

  /**
   * Get the coefficients for a low-pass biquad filter.
   * @private
   * @function
   * @param {number} Fs Sample rate (Hz)
   * @param {number} f0 Corner frequency (Hz)
   * @param {number} Q
   * @returns {Coefficients}
   */
  static _lpf(Fs, f0, Q) {
    const w0 = 2 * Math.PI * (f0 / Fs)
    const cosW0 = Math.cos(w0)
    const sinW0 = Math.sin(w0)
    const alpha = sinW0 / (2 * Q)

    const b0 = (1 - cosW0) / 2
    const b1 = 1 - cosW0
    const b2 = (1 - cosW0) / 2
    const a0 = 1 + alpha
    const a1 = -2 * cosW0
    const a2 = 1 - alpha
    return new Coefficients(b0, b1, b2, a0, a1, a2)
  }

  /**
   * Get the coefficients for a high-pass biquad filter.
   * @private
   * @function
   * @param {number} Fs Sample rate (Hz)
   * @param {number} f0 Corner frequency (Hz)
   * @param {number} Q
   * @returns {Coefficients}
   */
  static _hpf(Fs, f0, Q) {
    const w0 = 2 * Math.PI * (f0 / Fs)
    const cosW0 = Math.cos(w0)
    const sinW0 = Math.sin(w0)
    const alpha = sinW0 / (2 * Q)

    const b0 = (1 + cosW0) / 2
    const b1 = -(1 + cosW0)
    const b2 = (1 + cosW0) / 2
    const a0 = 1 + alpha
    const a1 = -2 * cosW0
    const a2 = 1 - alpha
    return new Coefficients(b0, b1, b2, a0, a1, a2)
  }

  /**
   * Get the coefficients for a band-pass biquad filter.
   * (constant 0 dB peak gain)
   * @private
   * @function
   * @param {number} Fs Sample rate (Hz)
   * @param {number} f0 Center frequency (Hz)
   * @param {number} Q
   * @returns {Coefficients}
   */
  static _bpf(Fs, f0, Q) {
    const w0 = 2 * Math.PI * (f0 / Fs)
    const cosW0 = Math.cos(w0)
    const sinW0 = Math.sin(w0)
    const alpha = sinW0 / (2 * Q)

    const b0 = alpha
    const b1 = 0
    const b2 = -alpha
    const a0 = 1 + alpha
    const a1 = -2 * cosW0
    const a2 = 1 - alpha
    return new Coefficients(b0, b1, b2, a0, a1, a2)
  }

  /**
   *  Get the coefficients for a notch biquad filter.
   * @private
   * @function
   * @param {number} Fs Sample rate (Hz)
   * @param {number} f0 Center frequency (Hz)
   * @param {number} Q
   * @returns {Coefficients}
   */
  static _notch(Fs, f0, Q) {
    const w0 = 2 * Math.PI * (f0 / Fs)
    const cosW0 = Math.cos(w0)
    const sinW0 = Math.sin(w0)
    const alpha = sinW0 / (2 * Q)

    const b0 = 1
    const b1 = -2 * cosW0
    const b2 = 1
    const a0 = 1 + alpha
    const a1 = -2 * cosW0
    const a2 = 1 - alpha
    return new Coefficients(b0, b1, b2, a0, a1, a2)
  }

  /**
   *  Get the coefficients for a peak biquad filter.
   * @private
   * @function
   * @param {number} Fs Sample rate (Hz)
   * @param {number} f0 Center frequency (Hz)
   * @param {number} Q
   * @param {number} dBgain Gain (Db)
   * @returns {Coefficients}
   */
  static _peakingEq(Fs, f0, Q, dBgain) {
    const A = Math.pow(10, dBgain / 40)
    const w0 = 2 * Math.PI * (f0 / Fs)
    const cosW0 = Math.cos(w0)
    const sinW0 = Math.sin(w0)
    const alpha = sinW0 / (2 * Q)

    const b0 = 1 + alpha * A
    const b1 = -2 * cosW0
    const b2 = 1 - alpha * A
    const a0 = 1 + alpha / A
    const a1 = -2 * cosW0
    const a2 = 1 - alpha / A
    return new Coefficients(b0, b1, b2, a0, a1, a2)
  }

  /**
   *  Get the coefficients for a low shelf biquad filter.
   * @private
   * @function
   * @param {number} Fs Sample rate (Hz)
   * @param {number} f0 Shelft midpoint frequency (Hz)
   * @param {number} Q
   * @param {number} dBgain Gain (Db)
   * @returns {Coefficients}
   */
  static _lowShelf(Fs, f0, Q, dBgain) {
    const A = Math.pow(10, dBgain / 40)
    const w0 = 2 * Math.PI * (f0 / Fs)
    const cosW0 = Math.cos(w0)
    const sinW0 = Math.sin(w0)
    const alpha = sinW0 / (2 * Q)
    const twoSqrtAalpha = 2 * Math.sqrt(A) * alpha

    const b0 = A * (A + 1 - (A - 1) * cosW0 + twoSqrtAalpha)
    const b1 = 2 * A * (A - 1 - (A + 1) * cosW0)
    const b2 = A * (A + 1 - (A - 1) * cosW0 - twoSqrtAalpha)
    const a0 = A + 1 + (A - 1) * cosW0 + twoSqrtAalpha
    const a1 = -2 * (A - 1 + (A + 1) * cosW0)
    const a2 = A + 1 + (A - 1) * cosW0 - twoSqrtAalpha
    return new Coefficients(b0, b1, b2, a0, a1, a2)
  }

  /**
   *  Get the coefficients for a high shelf biquad filter.
   * @private
   * @function
   * @param {number} Fs Sample rate (Hz)
   * @param {number} f0 Shelft midpoint frequency (Hz)
   * @param {number} Q
   * @param {number} dBgain Gain (Db)
   * @returns {Coefficients}
   */
  static _highShelf(Fs, f0, Q, dBgain) {
    const A = Math.pow(10, dBgain / 40)
    const w0 = 2 * Math.PI * (f0 / Fs)
    const cosW0 = Math.cos(w0)
    const sinW0 = Math.sin(w0)
    const alpha = sinW0 / (2 * Q)
    const twoSqrtAalpha = 2 * Math.sqrt(A) * alpha

    const b0 = A * (A + 1 + (A - 1) * cosW0 + twoSqrtAalpha)
    const b1 = -2 * A * (A - 1 + (A + 1) * cosW0)
    const b2 = A * (A + 1 + (A - 1) * cosW0 - twoSqrtAalpha)
    const a0 = A + 1 - (A - 1) * cosW0 + twoSqrtAalpha
    const a1 = 2 * (A - 1 - (A + 1) * cosW0)
    const a2 = A + 1 - (A - 1) * cosW0 - twoSqrtAalpha
    return new Coefficients(b0, b1, b2, a0, a1, a2)
  }
}
